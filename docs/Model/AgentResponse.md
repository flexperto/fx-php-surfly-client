# AgentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent_id** | **int** | ID of the agent | [optional] 
**agent_name** | **string** | name of the agent | [optional] 
**agent_email** | **string** | email address of the agent | [optional] 
**last_login** | **string** | date and time when user logged in or agent token has been generated | [optional] 
**role** | **string** | agent role. Possible roles: admin, manager and agent | [optional] 
**available** | **bool** | agent availability status (boolean: true or false) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


