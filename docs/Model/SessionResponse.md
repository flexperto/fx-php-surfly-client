# SessionResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**session_id** | **string** | unique ID of the session | [optional] 
**formatted_id** | **string** | unique formatted ID of the session | [optional] 
**agent_id** | **int** | ID of the agent | [optional] 
**follower_link** | **string** | the link that must be opened in a follower browser | [optional] 
**leader_link** | **string** | the link that must be opened in a leader (controlling) browser. It has a 2 minute time out attached to it, if not used within that time, the session will be closed. | [optional] 
**start_time** | [**\DateTime**](\DateTime.md) | time the session started | [optional] 
**duration** | **int** | length of the session (in seconds) | [optional] 
**participant_num** | **int** | number of session participants | [optional] 
**end_time** | [**\DateTime**](\DateTime.md) | time the session finished | [optional] 
**successful_start** | **bool** | Whether the session was technically successful or not | [optional] 
**pin** | **string** | 4-digit PIN code that can be used to join the session | [optional] 
**queued** | **bool** | Whether the session has been put in a waiting queue | [optional] 
**waiting_time** | **string** | time the session waited to start when it was in queue. will be null if itâ€™s never been in queue | [optional] 
**start_url** | **string** | page the session started from | [optional] 
**meta** | **object** | additional data attached to joining user | [optional] 
**tags** | **string[]** | array of attached session tags | [optional] 
**options** | [**\SurflyAPI\Client\Model\SessionOptionsResponse**](SessionOptionsResponse.md) |  | [optional] 
**cobro_server_name** | **string** | server name for multi-server installations | [optional] 
**opentok_archive_id** | **string** | Opentok archive ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


