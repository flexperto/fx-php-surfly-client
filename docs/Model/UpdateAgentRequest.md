# UpdateAgentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **string** | username of the agent | [optional] 
**agent_email** | **string** | email address of the agent | [optional] 
**role** | **string** | role of the agent, defaults to None which will create a regular agent. Other available roles: admin, manager. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


