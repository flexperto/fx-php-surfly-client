# SurflyAPI\Client\SessionsApi

All URIs are relative to *https://surfly.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sessionsGet**](SessionsApi.md#sessionsGet) | **GET** /sessions/ | List all Sessions
[**sessionsIDGet**](SessionsApi.md#sessionsIDGet) | **GET** /sessions/{ID}/ | Get session information
[**sessionsPost**](SessionsApi.md#sessionsPost) | **POST** /sessions/ | Create a session


# **sessionsGet**
> \SurflyAPI\Client\Model\SessionsResponse sessionsGet($active_session, $tags, $agent_id)

List all Sessions

List all sessions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$active_session = false; // bool | If set to true only ACTIVE Sessions will be returned
$tags = array("tags_example"); // string[] | If set, only Sessions with the given tags will be returned
$agent_id = 56; // int | If set, only Sessions of the given agnet will be returned

try {
    $result = $apiInstance->sessionsGet($active_session, $tags, $agent_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->sessionsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **active_session** | **bool**| If set to true only ACTIVE Sessions will be returned | [optional] [default to false]
 **tags** | [**string[]**](../Model/string.md)| If set, only Sessions with the given tags will be returned | [optional]
 **agent_id** | **int**| If set, only Sessions of the given agnet will be returned | [optional]

### Return type

[**\SurflyAPI\Client\Model\SessionsResponse**](../Model/SessionsResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionsIDGet**
> \SurflyAPI\Client\Model\SessionResponse sessionsIDGet($id)

Get session information

Get information about a session by ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = true; // bool | can be a formatted ID (e.g. 123-123-123), or an unformatted ID (e.g. fuSHr0sRQ1usugvheahwQ)

try {
    $result = $apiInstance->sessionsIDGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->sessionsIDGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **bool**| can be a formatted ID (e.g. 123-123-123), or an unformatted ID (e.g. fuSHr0sRQ1usugvheahwQ) |

### Return type

[**\SurflyAPI\Client\Model\SessionResponse**](../Model/SessionResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **sessionsPost**
> \SurflyAPI\Client\Model\CreateSessionResponse sessionsPost($session)

Create a session

Create a new session

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\SessionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session = new \SurflyAPI\Client\Model\CreateSessionRequest(); // \SurflyAPI\Client\Model\CreateSessionRequest | 

try {
    $result = $apiInstance->sessionsPost($session);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SessionsApi->sessionsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session** | [**\SurflyAPI\Client\Model\CreateSessionRequest**](../Model/CreateSessionRequest.md)|  |

### Return type

[**\SurflyAPI\Client\Model\CreateSessionResponse**](../Model/CreateSessionResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

