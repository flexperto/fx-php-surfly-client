# SurflyAPI\Client\AgentsApi

All URIs are relative to *https://surfly.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**agentsAGENTIDDelete**](AgentsApi.md#agentsAGENTIDDelete) | **DELETE** /agents/{AGENT_ID}/ | Delete an Agent
[**agentsAGENTIDGet**](AgentsApi.md#agentsAGENTIDGet) | **GET** /agents/{AGENT_ID}/ | Get Agent information
[**agentsAGENTIDPut**](AgentsApi.md#agentsAGENTIDPut) | **PUT** /agents/{AGENT_ID}/ | Update an agent
[**agentsGet**](AgentsApi.md#agentsGet) | **GET** /agents/ | List all agents
[**agentsPost**](AgentsApi.md#agentsPost) | **POST** /agents/ | Create a new agent


# **agentsAGENTIDDelete**
> \SurflyAPI\Client\Model\DeleteAgentResponse agentsAGENTIDDelete($agent_id)

Delete an Agent

Remove an Agent

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\AgentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agent_id = 56; // int | id of the Agent that should be updated

try {
    $result = $apiInstance->agentsAGENTIDDelete($agent_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgentsApi->agentsAGENTIDDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent_id** | **int**| id of the Agent that should be updated |

### Return type

[**\SurflyAPI\Client\Model\DeleteAgentResponse**](../Model/DeleteAgentResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **agentsAGENTIDGet**
> \SurflyAPI\Client\Model\AgentResponse agentsAGENTIDGet($agent_id)

Get Agent information

Get information about a certain Agent

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\AgentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agent_id = 56; // int | id of the Agent

try {
    $result = $apiInstance->agentsAGENTIDGet($agent_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgentsApi->agentsAGENTIDGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent_id** | **int**| id of the Agent |

### Return type

[**\SurflyAPI\Client\Model\AgentResponse**](../Model/AgentResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **agentsAGENTIDPut**
> \SurflyAPI\Client\Model\AgentResponse agentsAGENTIDPut($agent_id, $agent)

Update an agent

Update an AGENT

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\AgentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agent_id = 56; // int | id of the Agent that should be updated
$agent = new \SurflyAPI\Client\Model\UpdateAgentRequest(); // \SurflyAPI\Client\Model\UpdateAgentRequest | The changes you want to apply to the Agent

try {
    $result = $apiInstance->agentsAGENTIDPut($agent_id, $agent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgentsApi->agentsAGENTIDPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent_id** | **int**| id of the Agent that should be updated |
 **agent** | [**\SurflyAPI\Client\Model\UpdateAgentRequest**](../Model/UpdateAgentRequest.md)| The changes you want to apply to the Agent |

### Return type

[**\SurflyAPI\Client\Model\AgentResponse**](../Model/AgentResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **agentsGet**
> \SurflyAPI\Client\Model\AgentsResponse agentsGet()

List all agents

List all AGENTS

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\AgentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->agentsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgentsApi->agentsGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\SurflyAPI\Client\Model\AgentsResponse**](../Model/AgentsResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **agentsPost**
> \SurflyAPI\Client\Model\CreateAgentResponse agentsPost($agent)

Create a new agent

Create a new AGENT

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: APIKey
$config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = SurflyAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new SurflyAPI\Client\Api\AgentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agent = new \SurflyAPI\Client\Model\CreateAgentRequest(); // \SurflyAPI\Client\Model\CreateAgentRequest | The agent to create

try {
    $result = $apiInstance->agentsPost($agent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgentsApi->agentsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent** | [**\SurflyAPI\Client\Model\CreateAgentRequest**](../Model/CreateAgentRequest.md)| The agent to create |

### Return type

[**\SurflyAPI\Client\Model\CreateAgentResponse**](../Model/CreateAgentResponse.md)

### Authorization

[APIKey](../../README.md#APIKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

