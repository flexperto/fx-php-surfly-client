#!/usr/bin/env bash
docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate \
    -i /local/spec.yaml \
    -l php \
    -o /local \
    --invoker-package "SurflyAPI\Client" \
    --model-package "Model" \
    --api-package "Api" \
    --git-user-id "flexperto" \
    --git-repo-id "fx-php-surfly-client" \
    --git-repo-base-url "https://casparbauer@bitbucket.org/" \
    --artifact-version "1.0.7" \

rm -r docs lib test
mv SwaggerClient-php/* ./
